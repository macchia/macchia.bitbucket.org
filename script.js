$(document).ready(function () {
    var offset = 285;
    $(window).scroll(function () {
        if ($(this).scrollTop() > offset) {
            $('.back-to-top').stop().animate({right: '0px'},100);
        } else {
            $('.back-to-top').stop().animate({right: '-58px'},100);
        }
    });

    $('.back-to-top').click(function (event) {
        event.preventDefault();
        $('html, body').animate({scrollTop: 0},300);
        return false;
    })
});


$(document).ready(function() {
    var s = $("#tabbed ul.menu");
    var pos = s.position();
    $(window).scroll(function() {
        if ($(this).scrollTop() > 237) {
            s.css({"position": "fixed", "top": "0px"});
        } else {
            s.css({"position": "static", "top": pos.top});
        }
    });
});